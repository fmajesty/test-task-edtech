Выполняйте сложение с помощью самых классных алгоритмов.

## Главные фичи
- Крутой фронтенд для суммирования двух чисел. 
- Лучшие алгоритмы. 
- Бекенд с API для фронтенда, чтобы вы могли спокойно сложить два числа.

## Быстрый старт: всё просто
```bash
$ docker-compose up -d 
```

## Разработка
- Необходимо установить [poetry](http://python-poetry.org)
- Docker
- node.js с npm

### Бекенд
```bash
$ cd backend
$ poetry install
$ poetry run python -m uvicorn app:app --reload
```

#### Тесты
```bash
$ poetry run python -m pytest
```

### Фронтенд
```bash
$ cd frontend
$ npm install
$ npm run serve
```