from pydantic import Field
from pydantic.main import BaseModel


class GetSumTwoNumbersScheme(BaseModel):
    number1: int = Field(ge=0)
    number2: int = Field(ge=0)


class ReturnSumTwoNumbersScheme(BaseModel):
    result: int = Field(ge=0)
