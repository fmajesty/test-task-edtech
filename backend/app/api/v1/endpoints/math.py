from fastapi import APIRouter
from starlette import status

from app.api.v1 import schemas

router = APIRouter()


def calc_sum_two_nums(number_one: int, number_two: int) -> int:
    """Calc sum of number_one and number_two"""
    return number_one + number_two


@router.post("/calc", status_code=status.HTTP_200_OK, response_model=schemas.ReturnSumTwoNumbersScheme)
async def sum_two_numbers(numbers: schemas.GetSumTwoNumbersScheme):
    return schemas.ReturnSumTwoNumbersScheme(**{"result": calc_sum_two_nums(numbers.number1, numbers.number2)})
