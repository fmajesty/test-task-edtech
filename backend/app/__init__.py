import uvicorn
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app.api.v1.api import api_router

app = FastAPI(title="test task", openapi_url=f"/api/v1/openapi.json")
app.add_middleware(
    CORSMiddleware,
    allow_origins="*",
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(api_router, prefix="/api/v1")


if __name__ == '__main__':
    uvicorn.run(app, host='localhost', port=8001)
