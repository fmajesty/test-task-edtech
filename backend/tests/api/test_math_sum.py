import pytest
from fastapi.testclient import TestClient


@pytest.mark.local
def test_math_sum(client: TestClient) -> None:
    data = {"number1": 1, "number2": 1}
    r = client.post("/api/v1/math/calc", json=data)
    response = r.json()
    assert response["result"] == 2, "I can't calculate sum of 1 and 1 :("


@pytest.mark.local
def test_math_sum_error_below_zero(client: TestClient) -> None:
    data = {"number1": -1, "number2": 1}
    r = client.post("/api/v1/math/calc", json=data)
    response = r.json()
    assert response["detail"][0]["msg"] == "ensure this value is greater than or equal to 0"

    data = {"number1": -1, "number2": -1}
    r = client.post("/api/v1/math/calc", json=data)
    response = r.json()
    assert response["detail"][0]["msg"] == "ensure this value is greater than or equal to 0"
